import { CovidData1, CovidRespone, covidTotal } from 'app/core/models/covid-data';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DashboardService {

  constructor(
    private http: HttpClient,
  ) {}
  url3 = `https://www.google-analytics.com/g/collect?v=2&tid=G-YX8J8EVB5X&gtm=2oeai0&_p=1196319448&sr=1366x768&ul=vi-vn&cid=1474499978.1631519311&_s=1&dl=https%3A%2F%2Ftiemchungcovid19.gov.vn%2Fportal&dt=C%E1%BB%95ng%20th%C3%B4ng%20tin%20ti%C3%AAm%20ch%E1%BB%A7ng%20Covid-19&sid=1634824724&sct=13&seg=1&en=page_view`;
  url = `https://ncovi.vnpt.vn/thongtindichbenh_v2?fbclid=IwAR2lD-2nagX9IXsaWrY338jFv5HXezz9gvnurVOixB6DRadQCXn1FeRSm20`;
  url2 = `https://static.pipezero.com/covid/data.json?fbclid=IwAR2zAIOrJ31AzQzJZH3CnY4KCtxEhOLxG8OX0nUex6HXEZ3nuw9zg-gIRiY`;
  getVnData(): Observable<CovidData1> {
    const vndata = this.http.get<CovidData1>(this.url);
    const vndata2 = this.http.get<CovidData1>(this.url2);
    const vndata3 = this.http.get<CovidData1>(this.url3);
    return vndata2;
  } 
  getAllCovidData(): Observable<CovidRespone> {
    const covidData = this.http.get<CovidRespone>(this.url2);
    return covidData;
  }
  // getCovidToday(): Observable<CovidRespone> {
  //   const covidToday = this.http.get<CovidRespone>(this.url2);
  //   return covidToday;
  // }
  // getCovidTotal(): Observable<CovidRespone> {
  //   const covidTotal = this.http.get<CovidRespone>(this.url2);
  //   return covidTotal;
  // }

}
