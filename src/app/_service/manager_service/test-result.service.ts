import { CONSTANT } from "./../../core/constants";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";
import { TestResult, TestResultObj, TestResultRespone } from "app/core/models/test-result";
@Injectable({
  providedIn: "root",
})
export class TestResultService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });
  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  getTestResult(params): Observable<TestResultRespone> {
    return this.http.get<TestResultRespone>(
      `${CONSTANT.BASE_URL}/v1/api/result/get-result-by-village?villageId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  getTestResultByUserId(params): Observable<TestResultRespone> {
    return this.http.get<TestResultRespone>(
      `${CONSTANT.BASE_URL}/v1/api/result/get-result?userId=${params}`,
      { headers: this.getHeaders() }
    );
  }
  testResultCreate(params, form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/result/create?userId=${params}`,
      form,
      { headers: this.getHeaders() }
    );
  }
}
