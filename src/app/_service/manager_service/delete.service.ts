import { CONSTANT } from "app/core/constants";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenStorageService } from "../JWT_service/token-storage.service";
@Injectable({
  providedIn: "root",
})
export class DeleteService {
  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.TokenStorageService.getToken()}`,
    });
  constructor(
    private http: HttpClient,
    private TokenStorageService: TokenStorageService
  ) {}

  deleteById(params): Observable<any> {
    return this.http.put<any>(
      `${CONSTANT.BASE_URL}/v1/api/${params}`,
      {},
      { headers: this.getHeaders(), responseType: 'string' as 'json' }
    );
  }
  deleteById2(params): Observable<any> {
    return this.http.put<any>(
      `${CONSTANT.BASE_URL}/v1/api/${params}`,
      {},
      { headers: this.getHeaders(), responseType: 'string' as 'json' }
    );
  }
}
