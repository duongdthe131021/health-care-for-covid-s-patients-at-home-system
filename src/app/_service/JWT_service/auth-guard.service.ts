import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { Injectable } from "@angular/core";
import { TokenStorageService } from "./token-storage.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuardService {
  constructor(
    private router: Router,
    private tokenStorage: TokenStorageService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.tokenStorage.getUser();
    const role = this.tokenStorage.getRole();
    if (
      (currentUser && role === "Super admin") ||
      (currentUser && role === "Trung tâm Y tế") ||
      (currentUser && role === "Trạm Y tế")
    ) {
      // if (
      //   window.location.hash.split("?")[0] == "#/exercise-edit" &&
      //   (role === "Trạm Y tế" || role === "Trung tâm Y tế")
      // ) {
      //   this.router.navigate(["**"]);
      //   return false;
      // }
      return true;
    } else {
      window.localStorage.clear();
      this.router.navigate(["/login"]);
      return false;
    }
  }
}
