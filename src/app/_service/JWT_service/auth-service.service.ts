import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CONSTANT } from "app/core/constants";
import { Observable } from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" }),
};

@Injectable({
  providedIn: "root",
})
export class AuthServiceService {
  constructor(private http: HttpClient) {}

  login(form: any): Observable<any> {
    const username = form.username;
    const password = form.password;
    return this.http.post(
      `${CONSTANT.BASE_URL}/anonymous/signin`,
      {
        username,
        password,
      },
      httpOptions
    );
  }
  // forgotPassword(form: any): Observable<any> {
  //   return this.http.post<any>(
  //     `${CONSTANT.BASE_URL}/v1/api/user/forgot-password`,
  //     form,
  //     httpOptions
  //   );
  // }
  
}
