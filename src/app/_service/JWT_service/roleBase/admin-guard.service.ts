import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { TokenStorageService } from "../token-storage.service";

@Injectable({
  providedIn: "root",
})
export class AdminGuardService {
  constructor(private tokenStorage: TokenStorageService) {}

  superAdminGuard() {
    const role = this.tokenStorage.getRole();
    if (role === "Super admin") {
      return true;
    } else {
      return false;
    }
  }
  adminGuard() {
    const role = this.tokenStorage.getRole();
    if (((role === "Super admin") || role === "Trung tâm Y tế")) {
      return true;
    } else {
      return false;
    }
  }
}
