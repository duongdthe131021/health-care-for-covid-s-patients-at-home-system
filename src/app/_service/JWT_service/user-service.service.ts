import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CONSTANT } from "app/core/constants";
import { UserResponse } from "app/core/models/user";
import { Observable } from "rxjs";
import { TokenStorageService } from "./token-storage.service";

@Injectable({
  providedIn: "root",
})
export class UserServiceService {
  CONSTANT = CONSTANT;
  constructor(
    private http: HttpClient,
    private tokenStorageService: TokenStorageService
  ) {}

  getHeaders = () =>
    new HttpHeaders({
      Authorization: `Bearer ${this.tokenStorageService.getToken()}`,
    });

  getPublicContent(): Observable<any> {
    return this.http.get(CONSTANT + "all", { responseType: "text" });
  }

  getCurrentUser(): Observable<UserResponse> {
    return this.http.get<UserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/current-user`,
      { headers: this.getHeaders() }
    );
  }
  userPasswordChange(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/user/change-password`,
      form,
      { headers: this.getHeaders() }
    );
  }

  userUpdate(form: any): Observable<UserResponse> {
    return this.http.post<UserResponse>(
      `${CONSTANT.BASE_URL}/v1/api/user/update`,
      form,
      { headers: this.getHeaders() }
    );
  }

  updateAvatar(request): Observable<any> {
    const formData = new FormData();
    formData.append("file", request.file);
    return this.http.post<any>("", {});
  }

  forgotPassword(form: any): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/user/forgot-password`,
      form,
      { headers: this.getHeaders(), responseType: "string" as "json" }
    );
  }
  resetPassword(form: any, params): Observable<any> {
    return this.http.post<any>(
      `${CONSTANT.BASE_URL}/v1/api/user/reset-password/${params}`,
      form,
      { headers: this.getHeaders(), responseType: "string" as "json" }
    );
  }
}
