import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-access-layout',
  templateUrl: './no-access-layout.component.html',
  styleUrls: ['./no-access-layout.component.css']
})
export class NoAccessLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
