
export interface DailyReportResponse {
  code: number;
  data: DailyReport[];
}
export interface DailyReports extends Array<DailyReport> {}
export interface ListOfReport extends Array<DailyReport> {}

export interface DailyReport {
  dailyReportId: number;
  userId: number;
  firstname: string;
  surname: string;
  lastname: string;
  date: string;
  time: string;
  tempetature: number;
  oxygen: number;
  medicines: Array<string>;
  symptoms: Array<string>;
  exercises: Array<string>;
  comment: string;
}
