//url1
export interface CovidData1 {
  data: VNResponse1;
}

export interface VNResponse1 {
  VN: VNData1[];
}

export interface VNData1 {
  confirmed: number;
  deaths: number;
  id: number;
  increase_confirmed: number;
  increase_deaths: number;
  increase_recovered: number;
  last_update: number;
  province_name: String;
  recovered: number;
  type: String;
}
//url2
export interface CovidRespone {
  locations: Provide[];
  overview: CovidOverview[];
  today: covidTotal;
  total: covidTotal;
}
export interface CovidOverview {
  avgCases7day: number;
  avgDeath7day: number;
  avgRecovered7day: number;
  cases: number;
  date: Date;
  death: number;
  recovered: number;
  treating: number;
}
export interface covidTotal {
  internal: CovidInternal;
  world: CovidInternal;
}
export interface CovidInternal {
  cases: number;
  death: number;
  recovered: number;
  treating: number;
}
export interface Provide {
  cases: number;
  casesToday: number;
  death: number;
  name: String;
  recovered: number;
  treating: number;
}
