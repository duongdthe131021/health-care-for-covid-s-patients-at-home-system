export interface CurrentUserResponse {
  code: number;
  data: CurrentUser;
  message: string;
}

export interface CurrentUserObj {
  data: CurrentUser;
}
export interface CurrentUser {
  userId: number;
  role: Role;
  username: string;
  password: string;
  email: string;
  phone: string;
  identityCard: string;
  firstname: string;
  surname: string;
  lastname: string;
  address: string;
  avatar: string;
  isActive: number;
  isOnline: number;
}

export interface Role {
  roleId: number;
  description: string;
  isActive: number;
}
