import { User } from "./user";

export interface MedicalStaffResponse {
  code: number;
  data: MedicalStaff[];
}
export interface MedicalStaffObject {
  data: MedicalStaff;
}
export interface MedicalStaff {
  user: User;
  userId: number;
  role: Role;
  username: String;
  password: String;
  email: String;
  phone: number;
  identityCard: null;
  firstname: String;
  surname: String;
  lastname: String;
  address: String;
  avatar: null;
  isActive: number;
  isOnline: number;
}
export interface Role {
  roleId: number;
  description: String;
  isActive: number;
}
