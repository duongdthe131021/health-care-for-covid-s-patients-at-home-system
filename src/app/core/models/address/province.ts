export interface ProvinceResponse {
  code: string;
  data: Province;
  message: string;
}
export interface ProvincesResponse {
  code: string;
  data: Province[];
  message: string;
}
export interface ListOfProvince extends Array<Province> {}

export interface Province {
  provinceId: number;
  name: string;
  isActive: number;
}
