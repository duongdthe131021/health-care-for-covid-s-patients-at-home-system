import { District } from "./district";
export interface VillageResponse {
  code: string;
  data: Village;
  message: string;
}
export interface VillagesResponse {
  code: string;
  data: Village[];
  message: string;
}
export interface ListOfVillage extends Array<Village> {}

export interface Village {
  villageId: number;
  name: string;
  district: District;
  isActive: number;
}
