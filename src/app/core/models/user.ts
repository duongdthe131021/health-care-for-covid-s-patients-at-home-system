import { Village } from "./address/village";
export interface UserResponse {
  code: string;
  data: User;
  message: string;
}
export interface ListOfUserResponse {
  code: number;
  data: ListOfUser;
  message: string;
}

export interface UserObj {
  data: ListOfUser;
}
export interface ListOfUser extends Array<User> {}
export interface User {
  medicalStaffId: number;
  userId: number;
  id: number;
  roleId: number;
  role: Role;
  dateOfBirth: Date;
  username: string;
  password: string;
  email: string;
  phone: string;
  identityCard: string;
  firstname: string;
  surname: string;
  lastname: string;
  address: string;
  avatar: string;
  isActive: number;
  isOnline: number;
  isAssign: number;
  village: Village;
  medicalStaff: Village;
  roleName: string;
  provinceName: string;
  provinceId: string;
  districtName: string;
  districtId: string;
  villageName: string;
  villageId: string;
  gender: string;
}

export interface Role {
  roleId: number;
  description: string;
  isActive: number;
}
