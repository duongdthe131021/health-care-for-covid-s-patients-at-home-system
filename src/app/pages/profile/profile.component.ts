import { Component, OnInit } from '@angular/core';
import { DailyReportResponse } from 'app/core/models/dailyReport';
import { CurrentUserResponse } from 'app/core/models/user-model';
import { TokenStorageService } from 'app/_service/JWT_service/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentUser: CurrentUserResponse;
    dailyReport: DailyReportResponse;

    fullName: string;

    constructor(private tokenStorageService: TokenStorageService) { 
        this.currentUser = this.tokenStorageService.getUser();
        this.dailyReport = this.tokenStorageService.getUser();
        this.fullName = this.currentUser.data.firstname + this.currentUser.data.lastname;
    }
    ngOnInit(){
        
    }
    
}
