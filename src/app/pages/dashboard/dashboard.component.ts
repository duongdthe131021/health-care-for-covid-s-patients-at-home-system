import {
  CovidInternal,
  CovidOverview,
  CovidRespone,
  covidTotal,
  Provide,
} from "./../../core/models/covid-data";
import { Component, OnInit } from "@angular/core";
import { VNData1 } from "app/core/models/covid-data";
import { ChartDataSets, ChartOptions, ChartType } from "chart.js";
import { Color, Label, SingleDataSet } from "ng2-charts";
import { DashboardService } from "app/_service/manager_service/dashboard.service";

@Component({
  selector: "dashboard-cmp",
  moduleId: module.id,
  templateUrl: "dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  vnData: VNData1[];
  covidOverviews: CovidOverview;
  covidTotalData: covidTotal;
  covidInternal: CovidInternal;
  covidWorld: CovidInternal;
  covidToday: CovidInternal;
  getLocationDatas: Provide[];
  getLocationData: Provide;

  covidLocationName = [];
  covidLocationCases = [];
  dateOfCovid = [];
  caseOfCovid = [];
  recoveredOfCovid = [];
  treatingOfCovid = [];
  a: number;

  //chart1
  public lineChartData: ChartDataSets[] = [
    {
      data: this.caseOfCovid,
      label: "Số ca mắc cả nước",
    },
    {
      data: this.recoveredOfCovid,
      label: "Số ca phục hồi cả nước",
    },
  ];

  public lineChartLabels: Label[] = this.dateOfCovid;

  public lineChartOptions: ChartOptions & { annotation?: any } = {
    responsive: true,
  };

  public lineChartColors: Color[] = [
    {
      borderColor: "red",
      backgroundColor: "rgba(255,0,0,0.3)",
    },
    {
      borderColor: "green",
      backgroundColor: "rgba(22,227,25,0.3)",
    },
  ];

  public lineChartLegend = true;
  public lineChartType = "line";
  public lineChartPlugins = [];
  //endOfChart1

  //chart2
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = ["Tử Vong", "Nhiễm bệnh"];
  public pieChartData: SingleDataSet = [98, 36778];
  public pieChartType: ChartType = "pie";
  public pieChartLegend = true;
  public pieChartPlugins = [];
  // public pieChartColors: Array<any> = [
  //   {
  //     backgroundColor: ["red", "#19d863"],
  //     // backgroundColor: 'red',
  //     borderColor: "rgba(135,206,250,1)",
  //   },
  //   {
  //     // backgroundColor: 'yellow',
  //     borderColor: "rgba(106,90,205,1)",
  //   },
  // ];

  //endOfChart2
  //chart3
  public lineChartData2: ChartDataSets[] = [
    {
      data: this.covidLocationCases,
      label: "Số ca mắc từng địa phương",
    },
  ];

  public lineChartLabels2: Label[] = this.covidLocationName;

  public lineChartOptions2: ChartOptions & { annotation?: any } = {
    responsive: true,
  };

  public lineChartColors2: Color[] = [
    {
      borderColor: "red",
    },
  ];
  public lineChartLegend2 = true;
  public lineChartType2 = "line";
  public lineChartPlugins2 = [];
  //endOfChart3

  constructor(private service: DashboardService) {}

  ngOnInit() {
    this.getDateOfCovidOverview();
    this.getCaseOfCovidOverview();
    this.getrecoveredOfCovidOfCovidOverview();
    // this.getTreatingOfCovidOfCovidOverview();
    this.getVNData();
    this.getTodayData();
    this.getTotalInternalData();
    // this.getTotalWorldData();
    this.getCovidLocationName();
    this.getCovidLocationCases();
    const provinceName = "Hà Nội";
    this.getProvinceData(provinceName);
    // this.getAllLocation();
  }
  getVNData() {
    this.service.getVnData().subscribe((data) => {
      console.log(data);
    });
  }
  // getAllLocation(){
  //   this.service.getAllCovidData().subscribe((data) => {
  //     data.Location.forEach((item) => {
  //       this.a ++;
  //     });
  //   });
  //   console.log(this.a);
  // }

  //Location
  getCovidLocationName() {
    this.service.getAllCovidData().subscribe((data) => {
      data.locations.forEach((item) => {
        this.covidLocationName.push(item.name);
      });
    });
  }
  getCovidLocationCases() {
    this.service.getAllCovidData().subscribe((data) => {
      // console.log(data);
      data.locations.forEach((item) => {
        this.covidLocationCases.push(item.cases);
      });
    });
  }
  //overviewData
  getDateOfCovidOverview() {
    this.service.getAllCovidData().subscribe((data) => {
      data.overview.forEach((item) => {
        this.dateOfCovid.push(item.date);
      });
    });
  }
  getCaseOfCovidOverview() {
    this.service.getAllCovidData().subscribe((data) => {
      data.overview.forEach((item) => {
        this.caseOfCovid.push(item.cases);
      });
    });
  }
  getrecoveredOfCovidOfCovidOverview() {
    this.service.getAllCovidData().subscribe((data) => {
      data.overview.forEach((item) => {
        this.recoveredOfCovid.push(item.recovered);
      });
    });
  }
  // getTreatingOfCovidOfCovidOverview() {
  //   this.service.getAllCovidData().subscribe((data) => {
  //     data.overview.forEach((item) => {
  //       this.treatingOfCovid.push(item.treating);
  //     });
  //   });
  //   console.log(this.treatingOfCovid);
  // }

  //TodayData
  getTodayData() {
    this.service.getAllCovidData().subscribe((data) => {
      this.covidToday = data.today.internal;
    });
  }
  //TotalData
  getTotalInternalData() {
    this.service.getAllCovidData().subscribe((data) => {
      this.covidInternal = data.total.internal;
      console.log(data);
    });
  }
  getTotalWorldData() {
    this.service.getAllCovidData().subscribe((data) => {
      this.covidWorld = data.total.world;
    });
  }

  getProvinceData(provinceName: string) {
    this.service.getAllCovidData().subscribe((data) => {
      this.getLocationDatas = data.locations;
      for (var location of this.getLocationDatas) {
        if (location.name.toLowerCase() === provinceName.toLowerCase()) {
          this.getLocationData = location;
        } 
      }
    });
  }
}
