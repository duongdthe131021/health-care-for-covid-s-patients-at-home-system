import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute } from "@angular/router";
import { Medicine } from "app/core/models/medicine";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { MedicinesService } from "app/_service/manager_service/medicines.service";
import { ReportService } from "app/_service/manager_service/report.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { DeleteComponent } from "../dialog/delete/delete.component";

@Component({
  selector: "app-medicines",
  templateUrl: "./medicines.component.html",
  styleUrls: ["./medicines.component.css"],
})
export class MedicinesComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  id: string;

  defaultImg = "/assets/images/default-medicine-image.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  medicines: Medicine[];
  p: number = 1;

  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;

  constructor(
    private tokenStorageService: TokenStorageService,
    private medicineService: MedicinesService,
    private matDialog: MatDialog,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id2;
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
    }
    this.getMedicine();
  }

  getMedicine() {
    this.blockUI.start();
    this.medicineService.getMedicine().subscribe(
      (data) => {
        this.blockUI.stop();
        this.medicines = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onDelete(id) {
    const dialogRef = this.matDialog.open(DeleteComponent, {
      data: {
        message: "Xóa dữ liệu?",
        type: "medicine",
        id: id,
        buttonText: {
          ok: "Xóa",
          cancel: "Hủy",
        },
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.getMedicine();
    });
  }
}
