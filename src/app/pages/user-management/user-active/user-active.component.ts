import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { ListOfDistrict } from "app/core/models/address/district";
import { ListOfProvince } from "app/core/models/address/province";
import { ListOfVillage } from "app/core/models/address/village";
import { ListOfUser, User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";
import { AccountService } from "app/_service/manager_service/account.service";
import { AddressService } from "app/_service/manager_service/address.service";
import { FileManagementService } from "app/_service/manager_service/file-management.service";
import { SearchService } from "app/_service/manager_service/search.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";

@Component({
  selector: "app-user-active",
  templateUrl: "./user-active.component.html",
  styleUrls: ["./user-active.component.css"],
})
export class UserActiveComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("username", { static: true }) usernameElement: ElementRef;
  @Input() name: String;
  formActive: FormGroup;
  p: number = 1;

  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  listOfVillage: ListOfVillage;
  listOfMedicalStaff: ListOfUser;

  medicalStaffs: ListOfUser;
  listOfUser: ListOfUser;
  user: User;
  listSearchUser: ListOfUser;
  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  defaultDistrictId: number;
  defaultVillageId: number;
  defaultUserRole = 3;
  constructor(
    private accountService: AccountService,
    private userService: UserServiceService,
    private router: Router,
    private fileService: FileManagementService,
    private tokenStorageService: TokenStorageService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private matDialog: MatDialog,
    private addressService: AddressService,
    private searchService: SearchService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getMedicalStaff(this.currentUser.districtId);
    } else {
      this.isMod = true;
      this.defaultVillageId = parseInt(this.currentUser.villageId);
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.defaultVillageId);
      } else {
        this.getPatientByVillage(this.defaultVillageId);
      }
      this.initForm();
    }
  }
  initForm(user?) {
    this.formActive = this.formBuilder.group({
      userId: [user?.userId],
      userName: [user?.username],
      gender: [user?.gender],
      isOnline: [user?.isOnline],
      isActive: [1],
      firstname: [user?.firstname],
      surname: [user?.surname],
      lastname: [user?.lastname],
      dob: [user?.dateOfBirth],
      mail: [user?.email],
      phone: [user?.phone],
      avatar: [user?.avatar],
      identityCard: [user?.identityCard],
      address: [user?.address],
      villageId: [user?.village?.villageId]
    });
  }
  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    this.listOfDistrict = null;
    this.listOfMedicalStaff = null;
    this.listOfUser = null;
    this.getDistrict(value);
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    this.listOfMedicalStaff = null;
    this.listOfUser = null;
    this.defaultDistrictId = value;
    this.getMedicalStaff(value);
  }

  getMedicalStaff(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.message.toLowerCase() == "success") {
          this.listOfMedicalStaff = data.data;
        } else {
          this.blockUI.stop();
          this.snackBar.open("Chưa có trạm y tế nào", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeVillage(value) {
    this.defaultVillageId = value;
    this.listOfUser = null;
    if (this.defaultUserRole == 3) {
      this.getDoctorByVillage(this.defaultVillageId);
    } else {
      this.getPatientByVillage(this.defaultVillageId);
    }
  }
  onchangeUserRole(roleId) {
    this.defaultUserRole = roleId;
    if (this.isSuperAdmin) {
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.defaultVillageId);
      } else {
        this.getPatientByVillage(this.defaultVillageId);
      }
    } else if (this.isAdmin) {
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.defaultVillageId);
      } else {
        this.getPatientByVillage(this.defaultVillageId);
      }
    } else {
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.currentUser.villageId);
      } else {
        this.getPatientByVillage(this.currentUser.villageId);
      }
    }
  }
  getDoctorByVillage(villageId) {
    this.blockUI.start();
    this.accountService.getDeactiveDoctor(villageId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfUser = data.data;
        // this.initForm(villageId, this.defaultUserRole);
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getPatientByVillage(villageId) {
    this.listOfUser = [];
    this.blockUI.start();
    this.accountService.getDeactivePatient(villageId).subscribe(
      (data) => {
        if (data) {
          this.blockUI.stop();
          this.listOfUser = data.data;
        } else {
          this.blockUI.stop();
          this.snackBar.open("Không có dữ liệu", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  onActive(userId) {
    this.blockUI.start();
    this.accountService.getUserById(userId).subscribe(
      (data) => {
        if (data) {
          this.user = data.data;
          this.initForm(this.user);
          if (this.user) {
            this.userService.userUpdate(this.formActive.value).subscribe(
              (data) => {
                this.blockUI.stop();
                if (data.code === "UPDATE_USER_SUCCESS") {
                  if (this.defaultUserRole == 3) {
                    this.getDoctorByVillage(this.defaultVillageId);
                  } else {
                    this.getPatientByVillage(this.defaultVillageId);
                  }
                  this.snackBar.open("Kích hoạt thành công", "Ẩn", {
                    duration: 3000,
                    horizontalPosition: "right",
                    verticalPosition: "bottom",
                  });
                } else {
                  this.blockUI.stop();
                  this.snackBar.open("Thất bại", "Ẩn", {
                    duration: 3000,
                    horizontalPosition: "right",
                    verticalPosition: "bottom",
                  });
                }
              },
              (error) => {
                this.blockUI.stop();
                this.snackBar.open("Lỗi hệ thống", "Ẩn", {
                  duration: 3000,
                  horizontalPosition: "right",
                  verticalPosition: "bottom",
                });
              }
            );
          }
        } else {
          this.blockUI.stop();
          this.snackBar.open("Không có dữ liệu", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  getFullName(lastName, surName, firstName) {
    if (lastName === "" && surName === "" && firstName === "") {
      return "Không có bác sĩ";
    }
    return lastName + " " + surName + " " + firstName;
  }
  getAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
  getWorkPlace(workPlace, village) {
    return workPlace + " - " + village;
  }
}
