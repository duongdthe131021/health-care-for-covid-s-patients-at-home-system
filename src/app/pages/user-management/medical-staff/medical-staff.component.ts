import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { ListOfVillage, Village } from "./../../../core/models/address/village";
import {
  District,
  ListOfDistrict,
} from "./../../../core/models/address/district";
import { Router } from "@angular/router";
import { AccountService } from "../../../_service/manager_service/account.service";
import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ListOfProvince, Province } from "app/core/models/address/province";
import { ListOfUser, User, UserResponse } from "app/core/models/user";
import { FileManagementService } from "app/_service/manager_service/file-management.service";
import * as FileSaver from "file-saver";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DeleteComponent } from "app/pages/dialog/delete/delete.component";
import { MatDialog } from "@angular/material/dialog";
import { AssignEditComponent } from "../assign-edit/assign-edit.component";
import { AddressService } from "app/_service/manager_service/address.service";
import { SearchService } from "app/_service/manager_service/search.service";
import { SearchComponent } from "app/pages/search/search.component";

@Component({
  selector: "app-medical-staff",
  templateUrl: "./medical-staff.component.html",
  styleUrls: ["./medical-staff.component.css"],
})
export class MedicalStaffComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("username", { static: true }) usernameElement: ElementRef;
  @Input() name: String;
  formExcel: FormGroup;
  searchForm: FormGroup;
  p: number = 1;

  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  listOfVillage: ListOfVillage;
  listOfMedicalStaff: ListOfUser;

  medicalStaffs: ListOfUser;
  listOfUser: ListOfUser;
  listSearchUser: ListOfUser;
  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  defaultVillageId: number;
  defaultDistrictId: number;
  defaultUserRole = 3;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private fileService: FileManagementService,
    private tokenStorageService: TokenStorageService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private matDialog: MatDialog,
    private addressService: AddressService,
    private searchService: SearchService
  ) {
    this.searchForm = this.formBuilder.group({
      name: [],
      villageId: [, Validators.required],
    });
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getMedicalStaffByAdmin(this.currentUser.districtId);
    } else {
      this.isMod = true;
      this.defaultVillageId = parseInt(this.currentUser.villageId);
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.defaultVillageId);
      } else {
        this.getPatientByVillage(this.defaultVillageId);
      }
      this.searchForm.patchValue({ villageId: this.currentUser.villageId });
    }
    this.initForm();
  }
  initForm() {
    if (this.isSuperAdmin) {
      this.formExcel = this.formBuilder.group({
        districtId: ["", Validators.required],
        villageId: ["", Validators.required],
        roleId: [this.defaultUserRole, Validators.required],
      });
    } else if (this.isAdmin) {
      this.formExcel = this.formBuilder.group({
        districtId: [this.currentUser.districtId],
        villageId: [],
        roleId: [this.defaultUserRole, Validators.required],
      });
    } else {
      this.formExcel = this.formBuilder.group({
        districtId: [this.currentUser.districtId],
        villageId: [this.currentUser.villageId],
        roleId: [this.defaultUserRole, Validators.required],
      });
    }
  }
  
  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    this.listOfDistrict = null;
    this.listOfMedicalStaff = null;
    this.listOfUser = null;
    this.getDistrict(value);
    this.formExcel.patchValue({ districtId: null, villageId: null });
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    this.listOfMedicalStaff = null;
    this.listOfUser = null;
    this.defaultDistrictId = value;
    this.formExcel.patchValue({
      districtId: value,
      villageId: null,
    });
    this.getMedicalStaffByAdmin(value);
  }
  //is Admin and superAdmin
  getMedicalStaffByAdmin(districtId) {
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        if (data.message.toLowerCase() == "success") {
          this.listOfMedicalStaff = data.data;
        } else {
          this.blockUI.stop();
          this.snackBar.open("Không có dữ liệu", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeVillage(value) {
    this.defaultVillageId = value;
    this.listOfUser = null;
    this.formExcel.patchValue({ villageId: value });
    this.searchForm.patchValue({ villageId: value });
    if (this.defaultUserRole == 3) {
      this.getDoctorByVillage(this.defaultVillageId);
    } else {
      this.getPatientByVillage(this.defaultVillageId);
    }
  }
  onchangeUserRole(roleId) {
    this.defaultUserRole = roleId;
    this.formExcel.patchValue({
      districtId: this.defaultDistrictId,
      roleId: roleId,
    });
    if (this.isSuperAdmin) {
      this.formExcel.patchValue({
        roleId: roleId,
      });
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.defaultVillageId);
      } else {
        this.getPatientByVillage(this.defaultVillageId);
      }
    } else if (this.isAdmin) {
      this.formExcel.patchValue({
        districtId: this.currentUser.districtId,
        roleId: roleId,
      });
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.defaultVillageId);
      } else {
        this.getPatientByVillage(this.defaultVillageId);
      }
    } else {
      this.formExcel.patchValue({
        districtId: this.currentUser.districtId,
        roleId: roleId,
      });
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.currentUser.villageId);
      } else {
        this.getPatientByVillage(this.currentUser.villageId);
      }
    }
    this.p = 1;
  }
  getDoctorByVillage(villageId) {
    this.blockUI.start();
    this.listOfUser = [];
    this.accountService.getDoctorByVillageId2(villageId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfUser = data.data;
        this.formExcel.patchValue({
          villageId: villageId,
        });
        // this.initForm(villageId, this.defaultUserRole);
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getPatientByVillage(villageId) {
    this.listOfUser = [];
    this.blockUI.start();
    this.accountService.getPatientByVillageId(villageId).subscribe(
      (data) => {
        if (data) {
          this.blockUI.stop();
          this.listOfUser = data.data;
          this.formExcel.patchValue({
            villageId: villageId,
          });
        } else {
          this.blockUI.stop();
          this.snackBar.open("Không có dữ liệu", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  getFullName(lastName, surName, firstName) {
    if (lastName === "" && surName === "" && firstName === "") {
      return "Không có bác sĩ";
    }
    return lastName + " " + surName + " " + firstName;
  }
  getAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
  getWorkPlace(workPlace, village) {
    return workPlace + " - " + village;
  }

  //assign
  onAssign(id: string) {
    this.router.navigate(["/assign"], { queryParams: { id: id } });
  }
  onAssignEdit(patientId, villageId, patientName) {
    const dialogRef = this.matDialog.open(AssignEditComponent, {
      data: {
        type: "assignEdit",
        patientId: patientId,
        patientName: patientName,
        villageId: villageId,
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (this.isAdmin || this.isSuperAdmin) {
        this.getPatientByVillage(villageId);
      } else {
        this.getPatientByVillage(this.currentUser.villageId);
      }
    });
  }
  //export
  fileExtension = ".xlsx";
  onDownloadFile(event: any): void {
    // console.log(this.formExcel.value);
    if (this.formExcel.invalid) {
      this.snackBar.open(
        "Xin hãy chọn trạm y tế, loại người dùng muốn tải",
        "Ẩn",
        {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        }
      );
      return;
    } else {
      this.blockUI.start();
      this.fileService.exportUserFile(this.formExcel.value).subscribe(
        (data) => {
          this.blockUI.stop();
          const blob = new Blob([data], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          FileSaver.saveAs(
            blob,
            "User-" + new Date().toLocaleDateString() + this.fileExtension
          );
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Tải xuống lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  onSearchUser() {
    console.log(this.searchForm.value);
    if (this.searchForm.invalid) {
      this.snackBar.open("Chưa nhập tên người dùng", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.searchService
        .searchUserByUserName(
          this.searchForm.get("name").value,
          this.currentUser.villageId
        )
        .subscribe(
          (data) => {
            this.blockUI.stop();
            this.listSearchUser = data;
            // const dialogRef = this.matDialog.open(SearchComponent, {
            //   data: {
            //     listUser: this.listSearchUser
            //   },
            // });

          },
          (err) => {
            this.blockUI.stop();
            this.snackBar.open("Tải xuống lỗi", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        );
    }
  }
  onDelete(id) {
    const dialogRef = this.matDialog.open(DeleteComponent, {
      data: {
        message: "Xóa dữ liệu?",
        type: "user",
        id: id,
        buttonText: {
          ok: "Xóa",
          cancel: "Hủy",
        },
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (this.defaultUserRole == 3) {
        this.getDoctorByVillage(this.defaultVillageId);
      } else {
        this.getPatientByVillage(this.defaultVillageId);
      }
    });
  }
}
