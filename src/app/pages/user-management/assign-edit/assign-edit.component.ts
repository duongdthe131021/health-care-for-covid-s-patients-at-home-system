import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router, ActivatedRoute } from "@angular/router";
import { ListOfUser, ListOfUserResponse, User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { AccountService } from "app/_service/manager_service/account.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";

@Component({
  selector: "app-assign-edit",
  templateUrl: "./assign-edit.component.html",
  styleUrls: ["./assign-edit.component.css"],
})
export class AssignEditComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formAssignEdit: FormGroup;

  ListOfDoctor: ListOfUser;
  listOfUserResponse: ListOfUserResponse;
  currentUser: User;
  isAdmin = false;

  defaultVillageId: number;
  defaultDoctorId: number;
  patientName: string;
  patientId: number;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<AssignEditComponent>,
    private accountService: AccountService,
    private tokenStorageService: TokenStorageService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe((params) => {
      this.defaultVillageId = params.id;
    });
    if(data){
      this.defaultVillageId = data.villageId;
      this.patientName = data.patientName;
      this.patientId = data.patientId;
    }
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.getDoctorByVillageId(this.defaultVillageId);
      this.isAdmin = true;
    }else{
      this.getDoctorByVillageId(this.currentUser.villageId);
    }
    this.initForm();
  }
  initForm() {
    this.formAssignEdit = this.formBuilder.group({
      patientId: [this.patientId, [Validators.required]],
      doctorId: ["", [Validators.required]],
    });
    console.log(this.formAssignEdit);
  }
  getDoctorByVillageId(villageId){
    this.blockUI.start();
    this.accountService.getDoctorByVillageId2(villageId).subscribe(
      (data) => {
        if (data) {
          this.blockUI.stop();
          this.listOfUserResponse = data;
          // if(data == [])
          this.ListOfDoctor = this.listOfUserResponse.data;
          // set first value from array
          
        } else {
          this.blockUI.stop();
          this.snackBar.open("Tải trang lỗi", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onAssign() {
    // console.log(this.formAssignEdit);
    if (this.formAssignEdit.invalid) {
      this.blockUI.stop();
      this.snackBar.open("Chưa chọn bác sĩ", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.accountService.editWorkAssign(this.formAssignEdit.value).subscribe(
        (res) => {
          if (res.code=="UPDATE_DOCTOR_SUCCESS") {
            this.blockUI.stop();
            this.snackBar.open("Thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
          this.dialogRef.close();
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
      
    }
  }

  getFullName(lastName, surName, firstName) {
    if (lastName === '' && surName === '' && firstName === '') {
      return 'Không có bác sĩ';
    }
    return lastName + " " + surName + " " + firstName;
  }
  getAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
  getWorkPlace(workPlace, village) {
    return workPlace + " - " + village;
  }
}
