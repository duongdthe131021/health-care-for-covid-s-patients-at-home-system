import { AddressService } from './../../../_service/manager_service/address.service';
import { AccountService } from "app/_service/manager_service/account.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { User } from "app/core/models/user";
import { formatDate } from "@angular/common";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";
import { Observable, Subscriber } from "rxjs";
import * as moment from "moment";
import { ListOfVillage } from 'app/core/models/address/village';
import { ThemeService } from 'ng2-charts';

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"],
})
export class UserProfileComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("file") file: any;
  formProfile: FormGroup;
  currentUser: User;

  backgroundImg = "assets/img/damir-bosnjak.jpg";
  avatar = "assets/images/default-avatar.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  fullName: string;
  address: string;
  dob: Date;
  listOfVillage: ListOfVillage;
  user: User;
  userId: string;
  formUpdate: FormGroup;
  lastName: string;
  firstName: string;
  surName: string;
  userVillageId: number;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserServiceService,
    private addressService: AddressService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private service: AccountService,
    private router: Router
  ) {
    this.route.queryParams.subscribe((params) => {
      this.userId = params.id;
    });
  }

  ngOnInit(): void {
    this.getUserById();
    this.initFormProfile();
  }
  initFormProfile(user?) {
    this.fullName = this.getFullName(
      user?.lastname,
      user?.surname,
      user?.firstname
    );
    this.address = this.getFullAddress(
      this.user?.address,
      this.user?.villageName,
      this.user?.districtName,
      this.user?.provinceName
    );
    this.formProfile = this.formBuilder.group({
      fullName: [
        this.fullName,
        [
          Validators.required,
          Validators.pattern(
            "^([a-z A-Z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếẾìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹs]+)$"
          ),
        ],
      ],
      userId: [user?.userId, Validators.required],
      userName: [user?.username, Validators.required],
      gender: [user?.gender],
      isOnline: [user?.isOnline],
      isActive: [user?.isActive],
      firstname: [user?.firstname],
      surname: [user?.surname],
      lastname: [user?.lastname],
      dob: [user?.dateOfBirth],
      mail: [
        user?.email,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
      ],
      phone: [
        user?.phone,
        [Validators.required, Validators.pattern("[0-9]{10}")],
      ],
      // phone: [user?.phone, Validators.required],
      avatar: [user?.avatar],
      identityCard: [user?.identityCard],
      address: [user?.address],
      villageId: [user?.village?.villageId]
    });
  }
  getUserById() {
    this.blockUI.start();
    this.service.getUserById(this.userId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.user = data.data;
        if (this.user.avatar != null) {
          this.avatar = this.baseImg + this.user.avatar;
        }
        this.userVillageId = this.user.village.villageId;
        this.getVillage(this.user.village.district.districtId);
        this.initFormProfile(this.user);
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  getVillage(districtId) {
    this.blockUI.start();
    this.addressService.getVillage(districtId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfVillage = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onPatchvillageId(value){
    this.formProfile.patchValue({
      villageId: value,
    });
  }
  onSubmit() {
    console.log(this.formProfile.value);
    // this.formProfile.get("dob").setValue(
    //   // moment(this.formProfile.get("dob").value).format("YYYY-MM-DD HH:mm:ss")
    //   moment(this.formProfile.get("dob").value).format("YYYY-MM-DD")
    // );
    if (this.formProfile.invalid) {
      this.snackBar.open("Thiếu thông tin", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      this.blockUI.stop();
      return;
    } else {
      this.blockUI.start();
      this.userService.userUpdate(this.formProfile.value).subscribe(
        (data) => {
          this.blockUI.stop();
          if (data.code === "UPDATE_USER_SUCCESS") {
            this.getUserById();
            this.snackBar.open("Cập nhật thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Cập nhật thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (error) => {
          this.blockUI.stop();
          if (error.error.error === "EMAIL_EXIST") {
            this.snackBar.open("Email đã tồn tại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.snackBar.open("Cập nhật thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        }
      );
    }
  }
  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.avatar = data;
        this.formProfile.patchValue({
          avatar: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }

  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }
  onChangephone(event) {
    console.log(event.target.value);
    this.formProfile.patchValue({
      username: event.target.value,
    });
  }
  convertFullName(name) {
    var names = name.split(" ");
    this.lastName = names[0].toString();
    this.firstName = names[names.length - 1].toString();
    this.surName = "";
    for (var i = 1; i != names.length; i++) {
      if (i != names.length - 1) {
        if (i == 1) {
          this.surName = this.surName + names[i];
        } else {
          this.surName = this.surName + " " + names[i];
        }
      }
    }
    this.formProfile.patchValue({
      firstname: this.firstName,
      surname: this.surName,
      lastname: this.lastName,
    });
  }
  nameSplit(fullName: string, index: number) {
    return fullName.split(" ")[index];
  }
  getFullName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  getFullAddress(address, villageName, districtName, provinceName) {
    return (
      address + ", " + villageName + " - " + districtName + " - " + provinceName
    );
  }
}
