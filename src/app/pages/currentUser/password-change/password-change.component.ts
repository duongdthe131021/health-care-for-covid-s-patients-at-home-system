import { UserServiceService } from "app/_service/JWT_service/user-service.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-password-change",
  templateUrl: "./password-change.component.html",
  styleUrls: ["./password-change.component.css"],
})
export class PasswordChangeComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formPassword: FormGroup;

  newPassword: boolean;
  reEnterPassword: boolean;
  oldPassword: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private tokenStorageService: TokenStorageService,
    private userService: UserServiceService,
    private router: Router
  ) {
    this.formPassword = this.formBuilder.group(
      {
        oldPassword: ["", [Validators.required, Validators.minLength(6)]],
        newPassword: ["", [Validators.required, Validators.minLength(6)]],
        reEnterPassword: ["", [Validators.required, Validators.minLength(6)]],
      }
      // {   validators: [Validation.match('password', 'confirmPassword')] }
    );
  }

  ngOnInit(): void {}

  toggleShowNewPassword() {
    this.newPassword = !this.newPassword;
  }

  toggleShowReEnterPassword() {
    this.reEnterPassword = !this.reEnterPassword;
  }
  toggleShowOldPassword() {
    this.oldPassword = !this.oldPassword;
  }

  onSubmit() {
    if (this.formPassword.invalid) {
      return;
    } else if (
      this.formPassword.get("newPassword").value !==
      this.formPassword.get("reEnterPassword").value
    ) {
      return;
    } else {
      this.blockUI.start();
      this.userService.userPasswordChange(this.formPassword.value).subscribe(
        (response) => {
          this.blockUI.stop();
          if (response.code === "CHANGE_PASSWORD_SUCCES") {
            this.logout();
            this.snackBar.open("Đổi mật khẩu thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else if (response.code === "OLD_PASSWORD_DOESNT_MATCH") {
            this.snackBar.open("Mật khẩu cũ không chính xác", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else if (response.code === "REPASSWORD_DOESNT_MATCH_NEW_PASS") {
            this.snackBar.open("Mật khẩu mới không chính trùng nhau", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          } else {
            this.snackBar.open("Đổi mật khẩu thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (err) => {
          this.snackBar.open("Tạo mới thất bại", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }

  onCancel() {
    this.formPassword.reset();
  }
  logout(): void {
    this.tokenStorageService.signOut();
    this.router.navigate(["/login"]);
    // window.location.reload();
  }
}
