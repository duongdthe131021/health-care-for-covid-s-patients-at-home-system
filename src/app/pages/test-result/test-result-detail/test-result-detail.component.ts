import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TestResultService } from "app/_service/manager_service/test-result.service";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";
import { TestResult } from "app/core/models/test-result";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { User } from "app/core/models/user";
@Component({
  selector: "app-test-result-detail",
  templateUrl: "./test-result-detail.component.html",
  styleUrls: ["./test-result-detail.component.css"],
})
export class TestResultDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  testResults: TestResult[];
  testResultsFilter: TestResult[];
  user: User;
  id: string;
  currentYear: number;
  emailNull = 'Không có email';
  defaultTestNumber = 1;
  constructor(
    private testResultService: TestResultService,
    private snackBar: MatSnackBar,
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
    });
  }

  ngOnInit(): void {
    this.getTestResultByUserId();
  }

  onChangeTestNumber(testNumber) {
    this.defaultTestNumber = testNumber;
    this.testResultsFilter = this.testResults.filter(
      (item) => item.numberTest == this.defaultTestNumber
    );
  }

  getTestResultByUserId() {
    this.blockUI.start();
    this.testResultService.getTestResultByUserId(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        this.testResults = data.data;
        this.user = this.testResults[0].user;
        this.testResultsFilter = this.testResults.filter(
          (item) => item.numberTest == 1
        );
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  converName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  converGender(gender) {
    if (gender === "1") {
      return "Nam";
    } else if (gender === "2") {
      return "Nữ";
    } else {
      return "Không rõ";
    }
  }

  converAge(yearOfBirth) {
    this.currentYear = new Date().getFullYear();
    var year = yearOfBirth.substr(0, 4);
    var age = this.currentYear - year;
    return age;
  }
  converAddress(village, district, province) {
    return village + " - " + district + " - " + province;
  }
  converTestResult(testResult){
    if(testResult == "1"){
      return 'Dương tính';
    } else{
      return 'Âm tính';
    }
  }
}
