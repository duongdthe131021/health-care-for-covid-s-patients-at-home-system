import { AccountService } from "app/_service/manager_service/account.service";
import { FileManagementService } from "./../../_service/manager_service/file-management.service";
import { TokenStorageService } from "./../../_service/JWT_service/token-storage.service";
import { TestResultService } from "./../../_service/manager_service/test-result.service";
import { TestResult } from "./../../core/models/test-result";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ListOfUser, User } from "app/core/models/user";
import * as XLSX from "xlsx";
import * as FileSaver from "file-saver";
import { AddressService } from "app/_service/manager_service/address.service";
import { Village } from "app/core/models/address/village";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DeleteComponent } from "../dialog/delete/delete.component";
import { MatDialog } from "@angular/material/dialog";
import { ListOfProvince } from "app/core/models/address/province";
import { ListOfDistrict } from "app/core/models/address/district";

@Component({
  selector: "app-test-result",
  templateUrl: "./test-result.component.html",
  styleUrls: ["./test-result.component.css"],
})
export class TestResultComponent implements OnInit {
  @ViewChild("villageId") myDiv: ElementRef;
  @BlockUI() blockUI: NgBlockUI;
  formExcel: FormGroup;

  testResults: TestResult[];
  testResultsFilter = [];
  p: number = 1;
  currentUser: User;
  listOfVillage: Village[];
  isSuperAdmin = false;
  isAdmin = false;
  isMod = false;
  defaultTestNumber: number;
  villageId: number;
  maxNumberTest: number;
  numberTest: number[] = [];
  number = 1;
  listOfProvince: ListOfProvince;
  listOfDistrict: ListOfDistrict;
  listOfMedicalStaff: ListOfUser;
  defaultProvinceId: number;
  defaultDistrictId: number;
  defaultVillageId: number;

  constructor(
    private testResultService: TestResultService,
    private snackBar: MatSnackBar,
    private tokenStorageService: TokenStorageService,
    private fileService: FileManagementService,
    private accountService: AccountService,
    private addressService: AddressService,
    private formBuilder: FormBuilder,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();

    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
      this.getProvince();
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
      this.getMedicalStaffByAdmin(this.currentUser.districtId);
    } else {
      this.isMod = true;
      this.defaultVillageId = parseInt(this.currentUser.villageId);
      this.getTestResult(this.defaultVillageId);
    }
    this.initForm();
  }

  //superAdmin
  getProvince() {
    this.blockUI.start();
    this.addressService.getProvince().subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfProvince = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeProvinceId(value) {
    this.numberTest = [];
    this.listOfDistrict = null;
    this.listOfMedicalStaff = null;
    this.testResultsFilter = null;
    this.defaultDistrictId = null;
    this.defaultProvinceId = value;
    this.formExcel.patchValue({
      provinceId: value,
      districtId: null,
      villageId: null,
    });
    this.getDistrict(value);
  }
  getDistrict(provinceId) {
    this.blockUI.start();
    this.addressService.getDistrict(provinceId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.listOfDistrict = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.defaultDistrictId = null;
        this.defaultVillageId = null;
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onChangeDistrictId(value) {
    this.numberTest = [];
    this.p = 1;
    this.listOfMedicalStaff = null;
    this.testResultsFilter = null;
    this.defaultDistrictId = value;
    this.getMedicalStaffByAdmin(value);
    this.formExcel.patchValue({
      districtId: value,
      villageId: null,
    });
  }
  //isAdmin

  getMedicalStaffByAdmin(districtId) {
    // this.ListOfMedicalStaff = [];
    this.blockUI.start();
    this.accountService.getMedicalStaff(districtId).subscribe((data) => {
      this.blockUI.stop();
      if (data.data.length != 0) {
        this.listOfMedicalStaff = data.data;
        this.formExcel.patchValue({
          villageId: this.defaultVillageId,
        });
      } else {
        this.blockUI.stop();
        this.defaultVillageId = null;
        this.defaultTestNumber = null;
        this.snackBar.open("Chưa có trạm y tế nào", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    });
  }
  onChangeMedicalStaff(value) {
    this.numberTest = [];
    this.defaultVillageId = value;
    this.formExcel.patchValue({ villageId: value });
    this.getTestResult(value);
  }

  //endOfAdmin

  onChangeTestNumber(testNumber) {
    this.p = 1;
    this.defaultTestNumber = testNumber;
    this.testResultsFilter = this.testResults.filter(
      (item) => item.numberTest == this.defaultTestNumber
    );
    this.formExcel.patchValue({ numberTest: this.defaultTestNumber });
  }
  getTestResult(villageId) {
    this.blockUI.start();
    this.numberTest = [];
    // this.testResults=[];
    // this.testResultsFilter=[];
    var numberTest: number[] = [];
    this.testResultService.getTestResult(villageId).subscribe(
      (data) => {
        this.blockUI.stop();
        this.testResults = data.data;
        // getTestNumberFilter
        for (var i = 0; i < this.testResults.length; i++) {
          numberTest.push(this.testResults[i]?.numberTest);
        }
        this.maxNumberTest = Math.max(...numberTest);
        for (var i = 1; i <= this.maxNumberTest; i++) {
          this.numberTest.push(i);
        }
        this.defaultTestNumber = this.numberTest[0];

        //getTestResultByTestNumber
        this.testResultsFilter = this.testResults.filter(
          (item) => item.numberTest == this.defaultTestNumber
        );
        console.log(this.numberTest[0]);
        this.initForm(villageId);
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }

  initForm(villageId?) {
    if (this.isSuperAdmin) {
      this.formExcel = this.formBuilder.group({
        provinceId: [this.defaultProvinceId, Validators.required],
        districtId: [this.defaultDistrictId, Validators.required],
        villageId: [this.defaultVillageId, Validators.required],
        numberTest: [this.defaultTestNumber, Validators.required],
      });
    } else {
      this.formExcel = this.formBuilder.group({
        provinceId: [this.currentUser.provinceId, Validators.required],
        districtId: [this.currentUser.districtId, Validators.required],
        villageId: [
          this.isAdmin ? villageId : this.currentUser.villageId,
          Validators.required,
        ],
        numberTest: [this.defaultTestNumber, Validators.required],
      });
    }
  }

  //export
  fileExtension = ".xlsx";
  onDownloadFile(event: any): void {
    console.log(this.formExcel.value);
    if (this.formExcel.invalid) {
      this.snackBar.open("Xin hãy chọn trạm y tế muốn tải", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.fileService.exportCovidResultFile(this.formExcel.value).subscribe(
        (data) => {
          this.blockUI.stop();
          const blob = new Blob([data], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          FileSaver.saveAs(
            blob,
            "Covid_test_result_" +
              new Date().toLocaleDateString() +
              this.fileExtension
          );
        },
        (err) => {
          this.blockUI.stop();
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
        }
      );
    }
  }
  openDialog(id) {
    const dialogRef = this.matDialog.open(DeleteComponent, {
      data: {
        message: "Xóa dữ liệu?",
        villageId: this.defaultVillageId,
        type: "testResult",
        id: id,
        numberTest: this.defaultTestNumber,
        buttonText: {
          ok: "Xóa",
          cancel: "Hủy",
        },
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.getTestResult(this.defaultVillageId);
    });
    // dialogRef.afterClosed().subscribe((data) => {
    //   if (data) {
    //     this.testResultsFilter = data;
    //   }
    // });
  }
  convertName(lastName, surName, firstName) {
    return lastName + " " + surName + " " + firstName;
  }
  converTestResult(testResult) {
    if (testResult == "1") {
      return "Dương tính";
    } else {
      return "Âm tính";
    }
  }
}
