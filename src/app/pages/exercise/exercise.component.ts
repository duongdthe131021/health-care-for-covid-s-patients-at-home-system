import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Component, OnInit } from "@angular/core";
import { Exercise } from "app/core/models/exercise";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ExerciseService } from "app/_service/manager_service/exercise.service";
import { MatDialog } from "@angular/material/dialog";
import { DeleteComponent } from "../dialog/delete/delete.component";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";

@Component({
  selector: "app-exercise",
  templateUrl: "./exercise.component.html",
  styleUrls: ["./exercise.component.css"],
})
export class ExerciseComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  defaultImg = "/assets/images/default-exercise.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  exercises: Exercise[];
  p: number = 1;

  currentUser: User;
  isSuperAdmin = false;
  isAdmin = false;

  constructor(
    private tokenStorageService: TokenStorageService,
    private exerciseService: ExerciseService,
    private snackBar: MatSnackBar,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.currentUser = this.tokenStorageService.getUser();
    if (this.currentUser.roleName === "Super admin") {
      this.isSuperAdmin = true;
    } else if (this.currentUser.roleName === "Trung tâm Y tế") {
      this.isAdmin = true;
    }
    this.getExercise();
  }

  getExercise() {
    this.blockUI.start();
    this.exerciseService.getExercise().subscribe(
      (data) => {
        this.blockUI.stop();
        this.exercises = data.data;
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onDelete(id) {
    const dialogRef = this.matDialog.open(DeleteComponent, {
      data: {
        message: "Xóa dữ liệu?",
        type: "exercise",
        id: id,
        buttonText: {
          ok: "Xóa",
          cancel: "Hủy",
        },
      },
    });
    dialogRef.afterClosed().subscribe((data) => {
      this.getExercise();
    });
  }

}
