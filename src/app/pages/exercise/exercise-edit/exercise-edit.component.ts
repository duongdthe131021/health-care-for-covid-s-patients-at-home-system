import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ExerciseService } from "app/_service/manager_service/exercise.service";
import { Exercise } from "app/core/models/exercise";
import { Observable, Subscriber } from "rxjs";
import { User } from "app/core/models/user";
import { TokenStorageService } from "app/_service/JWT_service/token-storage.service";

@Component({
  selector: "app-exercise-edit",
  templateUrl: "./exercise-edit.component.html",
  styleUrls: ["./exercise-edit.component.css"],
})
export class ExerciseEditComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  formUpdate: FormGroup;

  exercise: Exercise;
  id: string;
  defaultImg = "/assets/images/default-exercise.jpg";
  baseImg =
    "https://healthcaresystemstorage.s3.us-east-2.amazonaws.com/images/";
  currentUser: User;

  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private exerciseService: ExerciseService
  ) {
    this.route.queryParams.subscribe((params) => {
      this.id = params.id;
    });
    this.currentUser = this.tokenStorageService.getUser();
    if (
      this.currentUser.roleName === "Trung tâm Y tế" ||
      this.currentUser.roleName === "Trạm Y tế"
    ) {
      this.router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    this.getExerciseById();
    this.initFormUpdate();
  }
  initFormUpdate(exercise?) {
    this.formUpdate = this.formBuilder.group({
      id: [this.id, Validators.required],
      name: [exercise?.name, [Validators.required, Validators.minLength(3)]],
      thumbnail: [exercise?.thumbnail, Validators.required],
      description: [exercise?.description, Validators.required],
      isActive: [1, Validators.required],
    });
  }
  getExerciseById() {
    this.blockUI.start();
    this.exerciseService.getExerciseById(this.id).subscribe(
      (data) => {
        this.blockUI.stop();
        this.exercise = data.data;
        this.initFormUpdate(this.exercise);
        if (this.exercise.thumbnail != null) {
          this.defaultImg = this.baseImg + this.exercise.thumbnail;
        }
      },
      (err) => {
        this.blockUI.stop();
        this.snackBar.open("Lỗi hệ thống", "Ẩn", {
          duration: 3000,
          horizontalPosition: "right",
          verticalPosition: "bottom",
        });
      }
    );
  }
  onSubmit(id: string) {
    if (this.formUpdate.invalid) {
      this.snackBar.open("Thiếu hoặc sai thông tin cập nhật", "Ẩn", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
      });
      return;
    } else {
      this.blockUI.start();
      this.exerciseService.exerciseUpdate(this.formUpdate.value).subscribe(
        (res) => {
          if (res.code == "SUCCESS") {
            this.blockUI.stop();
            this.snackBar.open("Cập nhật thành công", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
            this.router.navigate(["/exercise-detail"], {
              queryParams: { id: id },
            });
          } else {
            this.blockUI.stop();
            this.snackBar.open("Cập nhật thất bại", "Ẩn", {
              duration: 3000,
              horizontalPosition: "right",
              verticalPosition: "bottom",
            });
          }
        },
        (error) => {
          this.snackBar.open("Lỗi hệ thống", "Ẩn", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "bottom",
          });
          this.blockUI.stop();
        }
      );
    }
  }
  onSelectFile(event) {
    const file = event.target.files[0];
    this.convertToBase64(file);
  }
  convertToBase64(file: File) {
    const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe(
      (data) => {
        this.defaultImg = data;
        this.formUpdate.patchValue({
          thumbnail: data,
        });
      },
      (error) => {
        alert(error);
      }
    );
  }

  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };
    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }
  onCancel(id: string) {
    this.router.navigate(["/exercise-detail"], { queryParams: { id: id } });
  }
}
