import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Component, Inject, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatSnackBar } from "@angular/material/snack-bar";
import { UserServiceService } from "app/_service/JWT_service/user-service.service";

@Component({
  selector: "app-confirm-dialog",
  templateUrl: "./confirm-dialog.component.html",
  styleUrls: ["./confirm-dialog.component.css"],
})
export class ConfirmDialogComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  message: string = "Bạn có chắc chắn muốn thay đổi mật khẩu?";
  subMessage: string = "Bạn sẽ tự động đăng xuất sau khi thay đổi mật khẩu!";
  confirmButtonText = "Lưu";
  cancelButtonText = "Hủy";
  apiData: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    private userService: UserServiceService,
    private snackBar: MatSnackBar
  ) {
    if (data) {
      this.message = data.message || this.message;
      if (data.buttonText) {
        this.confirmButtonText = data.buttonText.ok || this.confirmButtonText;
        this.cancelButtonText = data.buttonText.cancel || this.cancelButtonText;
      }
    }
  }
  ngOnInit(): void {}
  onConfirm(): void {
    this.dialogRef.close(true);
  }
}
